using ParallelHW.Core.Entities;

namespace ParallelHW.Core.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
    }
}