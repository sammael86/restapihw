using System.Collections.Generic;
using ParallelHW.Core.Entities;

namespace ParallelHW.Core.Generators
{
    public abstract class AbstractDataGenerator
    {
        public List<Customer> Customers { get; protected set; }
        public string DataFilePath { get; protected set; }
        public abstract void Generate();
    }
}