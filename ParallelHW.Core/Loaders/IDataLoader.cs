﻿namespace ParallelHW.Core.Loaders
{
    public interface IDataLoader
    {
        void LoadData();
    }
}