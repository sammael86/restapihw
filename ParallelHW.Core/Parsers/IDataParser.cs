﻿namespace ParallelHW.Core.Parsers
{
    public interface IDataParser<out T>
    {
        T Parse();
    }
}