namespace ParallelHW.Core.Entities
{
    public class Customer
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public override string ToString()
            => $"ID: {Id}, Full name: {FullName}, Email: {Email}, Phone: {Phone}";

        public string ToJson()
            => $"{{\"id\":{Id},\"fullName\":\"{FullName}\",\"email\":\"{Email}\",\"phone\":\"{Phone}\"}}";
    }
}