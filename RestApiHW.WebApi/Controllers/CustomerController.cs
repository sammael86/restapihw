﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ParallelHW.Core.Entities;
using ParallelHW.DataAccess.Repositories;

namespace RestApiHW.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ILogger<CustomerController> _logger;

        public CustomerController(ILogger<CustomerController> logger)
            => _logger = logger;

        /// <summary>
        /// Get Customer by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Customer</returns>
        /// <response code="200">Returns Customer</response>
        /// <response code="404">If a Customer with this Id does not exists</response>
        [HttpGet]
        [Route("/users/{id}")]
        public async Task<ActionResult<Customer>> GetCustomer(int id)
        {
            var customerRepository = new CustomerRepository();
            var customer = await customerRepository.GetCustomerByIdAsync(id);

            if (customer == null)
            {
                var message = $"Requested Customer with ID {id} not found";
                _logger.LogInformation(message);
                return NotFound(message);
            }

            _logger.LogInformation($"Successfully requested Customer with ID {id}");
            return Ok(customer);
        }

        /// <summary>
        /// Add Customer to Database
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Added Customer</returns>
        /// <response code="200">Returns added Customer</response>
        /// <response code="409">If a Customer with this Id already exists</response>
        [HttpPost]
        [Route("/users")]
        public async Task<ActionResult<Customer>> AddCustomer(Customer customer)
        {
            var customerRepository = new CustomerRepository();

            try
            {
                await customerRepository.AddCustomerAsync(customer);
            }
            catch
            {
                var message = $"Customer with ID {customer.Id} already exist";
                _logger.LogInformation(message);
                return Conflict(message);
            }

            _logger.LogInformation($"Customer with ID {customer.Id} successfully added");
            return Ok(customer);
        }
    }
}