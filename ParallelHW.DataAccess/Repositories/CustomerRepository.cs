using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ParallelHW.Core.Entities;
using ParallelHW.Core.Repositories;

namespace ParallelHW.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private readonly DataContext _db;

        public CustomerRepository()
            => _db = new DataContext();

        public void AddCustomer(Customer customer)
        {
            _db.Customers.Add(new Customer
            {
                FullName = customer.FullName,
                Email = customer.Email,
                Phone = customer.Phone
            });
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            await _db.Customers.AddAsync(customer);
            await _db.Database.OpenConnectionAsync();
            try
            {
                await _db.Database.ExecuteSqlRawAsync("SET IDENTITY_INSERT dbo.Customers ON");
                await _db.SaveChangesAsync();
                await _db.Database.ExecuteSqlRawAsync("SET IDENTITY_INSERT dbo.Customers OFF");
            }
            finally
            {
                await _db.Database.CloseConnectionAsync();
            }
        }
        
        public async Task<Customer> GetCustomerByIdAsync(int id)
            => await _db.Customers.FindAsync(id);

        public void Save()
            => _db.SaveChanges();
    }
}