﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ParallelHW.Core.Parsers;
using ParallelHW.Core.Entities;
using System.Xml;

namespace ParallelHW.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly string _dataFilePath;

        public XmlParser(string dataFilePath)
        {
            _dataFilePath = $"{dataFilePath}.xml";
        }

        public List<Customer> Parse()
        {
            Console.WriteLine("Parsing XML file");
            using var reader = new StreamReader(_dataFilePath);
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(reader);
            return xmlDocument.SelectNodes("//Customer")
                .Cast<XmlNode>()
                .Select(node => new Customer()
                {
                    Email = node[nameof(Customer.Email)].InnerText,
                    FullName = node[nameof(Customer.FullName)].InnerText,
                    Id = int.Parse(node[nameof(Customer.Id)].InnerText),
                    Phone = node[nameof(Customer.Phone)].InnerText
                }).ToList();
        }
    }
}