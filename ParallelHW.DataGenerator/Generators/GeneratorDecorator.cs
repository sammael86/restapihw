﻿using ParallelHW.Core.Generators;

namespace ParallelHW.DataGenerator.Generators
{
    public abstract class GeneratorDecorator : AbstractDataGenerator
    {
        private readonly AbstractDataGenerator _generator;

        protected GeneratorDecorator(AbstractDataGenerator generator)
        {
            _generator = generator;
        }

        public override void Generate()
        {
            _generator.Generate();
            Customers = _generator.Customers;
            DataFilePath = _generator.DataFilePath;
        }
    }
}