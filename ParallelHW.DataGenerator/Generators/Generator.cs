﻿using System;
using ParallelHW.Core.Generators;

namespace ParallelHW.DataGenerator.Generators
{
    public class Generator : AbstractDataGenerator
    {
        private readonly int _dataCount;

        public Generator(int dataCount, string dataFilePath)
        {
            _dataCount = dataCount;
            DataFilePath = dataFilePath;
        }

        public override void Generate()
        {
            Console.WriteLine(" Generating data");
            Customers = RandomCustomerGenerator.Generate(_dataCount);
        }
    }
}