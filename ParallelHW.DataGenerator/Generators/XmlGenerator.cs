using System;
using System.IO;
using System.Xml.Serialization;
using ParallelHW.Core.Generators;
using ParallelHW.DataGenerator.Dto;

namespace ParallelHW.DataGenerator.Generators
{
    public class XmlGenerator : GeneratorDecorator
    {
        public XmlGenerator(AbstractDataGenerator generator)
            : base(generator)
        {
        }

        public override void Generate()
        {
            base.Generate();

            Console.WriteLine(" Generating XML file");

            using var stream = File.Create($"{DataFilePath}.xml");
            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Customers = Customers
            });

            Console.WriteLine(" XML file generated");
        }
    }
}