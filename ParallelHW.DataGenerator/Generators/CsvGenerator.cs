﻿using System;
using System.IO;
using CsvHelper;
using System.Globalization;
using ParallelHW.Core.Generators;

namespace ParallelHW.DataGenerator.Generators
{
    public class CsvGenerator : GeneratorDecorator
    {
        public CsvGenerator(AbstractDataGenerator generator)
            : base(generator)
        {
        }

        public override void Generate()
        {
            base.Generate();

            Console.WriteLine(" Generating CSV file");

            using var stream = new StreamWriter($"{DataFilePath}.csv");
            using var csv = new CsvWriter(stream, CultureInfo.InvariantCulture);
            csv.WriteRecords(Customers);
            
            Console.WriteLine(" CSV file generated");
        }
    }
}