﻿using System.Collections.Generic;
using System.Xml.Serialization;
using ParallelHW.Core.Entities;

namespace ParallelHW.DataGenerator.Dto
{
    [XmlRoot("Customers")]
    public class CustomersList
    {
        public List<Customer> Customers { get; set; }
    }
}