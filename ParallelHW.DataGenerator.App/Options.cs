﻿using System.Collections.Generic;
using CommandLine;
using CommandLine.Text;

namespace ParallelHW.DataGenerator.App
{
    public class Options
    {
        [Option('n', "filename", Default = "customers", Required = false,
            HelpText = "File name without extension")]
        public string DataFilePath { get; set; }

        [Option('d', "dc", Default = 100, Required = false, HelpText = "Amount of data to generate")]
        public int DataCount { get; set; }

        [Option('f', "format", Default = 0, Required = false, HelpText = "File format: 0 - xml, 1 - csv")]
        public DataFormat DataFormat { get; set; }

        [Usage]
        public static IEnumerable<Example> _
            => new List<Example>
            {
                new Example("Data generation example", new Options
                {
                    DataFilePath = "customers",
                    DataCount = 100,
                    DataFormat = DataFormat.Xml
                })
            };
    }

    public enum DataFormat
    {
        Xml,
        Csv
    }
}