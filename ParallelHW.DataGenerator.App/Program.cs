﻿using System;
using System.Diagnostics;
using CommandLine;
using ParallelHW.Core.Generators;
using ParallelHW.DataGenerator.Generators;

namespace ParallelHW.DataGenerator.App
{
    internal static class Program
    {
        private static string _dataFilePath;
        private static int _dataCount;
        private static DataFormat _dataFormat;

        private static void Main(string[] args)
        {
            if (LoadOptions(args).Tag == ParserResultType.NotParsed)
                return;

            Console.WriteLine($"Data generator started with process Id {Process.GetCurrentProcess().Id}...");

            AbstractDataGenerator generator = new Generator(_dataCount, _dataFilePath);

            if (_dataFormat == DataFormat.Xml)
                generator = new XmlGenerator(generator);
            if (_dataFormat == DataFormat.Csv)
                generator = new CsvGenerator(generator);

            generator.Generate();
        }

        private static ParserResult<Options> LoadOptions(string[] args)
            => Parser.Default.ParseArguments<Options>(args)
                .WithParsed(o =>
                {
                    _dataFilePath = o.DataFilePath;
                    _dataCount = o.DataCount;
                    _dataFormat = o.DataFormat;
                });
    }
}