﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ParallelHW.Core.Generators;

namespace RestApiHW.ConsoleApp
{
    internal static class Program
    {
        private static readonly string baseAddress = "https://localhost:5001/";

        private static async Task Main()
        {
            await MainLoop();
        }

        private static async Task MainLoop()
        {
            do
            {
                PrintMainMenu();
                switch (GetUserChoice(2))
                {
                    case 1:
                        var id = GetInt();
                        await GetCustomerAsync(id);
                        break;
                    case 2:
                        await AddRandomCustomerAsync();
                        break;
                    case 0:
                        return;
                }

                PrintContinueMenu();
                switch (GetUserChoice(1))
                {
                    case 1:
                        break;
                    case 0:
                        return;
                }
            } while (true);
        }

        private static void PrintMainMenu()
        {
            Console.WriteLine("Select action:");
            Console.WriteLine("\t1. Select user by Id");
            Console.WriteLine("\t2. Add randomly generated user");
            Console.WriteLine("\t0. Exit");
        }

        private static void PrintContinueMenu()
        {
            Console.WriteLine("Select:");
            Console.WriteLine("\t1. Continue");
            Console.WriteLine("\t0. Exit");
        }

        private static int GetUserChoice(int maxChoice)
        {
            Console.Write("\nYour choice: ");
            int userChoice;
            var attemptCounter = 0;
            do
            {
                if (attemptCounter > 0)
                    Console.Write($"\nYou must enter a number between 0 to {maxChoice}! Your choice: ");
                attemptCounter++;
            } while (!int.TryParse(Console.ReadLine(), out userChoice) || userChoice < 0 || userChoice > maxChoice);

            return userChoice;
        }

        private static int GetInt()
        {
            Console.Write("\nEnter Id: ");
            int userId;
            var attemptCounter = 0;
            do
            {
                if (attemptCounter > 0)
                    Console.Write("\nYou must enter a number greater than 0! Enter Id: ");
                attemptCounter++;
            } while (!int.TryParse(Console.ReadLine(), out userId) || userId < 0);

            return userId;
        }

        private static async Task GetCustomerAsync(int id)
        {
            using var client = new HttpClient {BaseAddress = new Uri(baseAddress)};
            var response = await client.GetAsync($"users/{id}");
            var result = await response.Content.ReadAsStringAsync();
            Console.WriteLine(result);
        }

        private static async Task AddRandomCustomerAsync()
        {
            using var client = new HttpClient {BaseAddress = new Uri(baseAddress)};
            var customers = RandomCustomerGenerator.Generate(100);
            var rand = new Random().Next(0, 100);
            var content = new StringContent(customers[rand].ToJson(), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("users", content);
            var result = await response.Content.ReadAsStringAsync();
            Console.WriteLine(result);
        }
    }
}