﻿using System.Collections.Generic;
using CommandLine;
using CommandLine.Text;

namespace ParallelHW.Loader
{
    public class Options
    {
        [Option('n', "filename", Default = "customers", Required = false,
            HelpText = "File name without extension")]
        public string DataFilePath { get; set; }

        [Option('m', Default = "method", Required = false,
            HelpText = "'method' for Method-generator or path to the data generator app")]
        public string Method { get; set; }

        [Option('d', "dc", Default = 100, Required = false, HelpText = "Amount of data to generate")]
        public int DataCount { get; set; }

        [Option('c', "tc", Default = 8, Required = false, HelpText = "Number of threads to process the data")]
        public int ThreadsCount { get; set; }

        [Option('f', "format", Default = 0, Required = false, HelpText = "File format: 0 - xml, 1 - csv")]
        public DataFormat DataFormat { get; set; }


        [Option('t', "type", Default = 0, Required = false, HelpText = "0 - Threads, 1 - ThreadPool")]
        public ThreadType ThreadType { get; set; }

        [Usage]
        public static IEnumerable<Example> _
            => new List<Example>
            {
                new Example("Data generation by method call example (default)", new Options
                {
                    DataFilePath = "customers",
                    Method = "method",
                    DataCount = 100,
                    ThreadsCount = 8,
                    DataFormat = DataFormat.Xml,
                    ThreadType = ThreadType.Thread
                }),
                new Example("Data generation by process start example", new Options
                {
                    DataFilePath = "customers",
                    Method = "ParallelHW.DataGenerator.App.exe",
                    DataFormat = DataFormat.Csv
                })
            };
    }

    public enum ThreadType
    {
        Thread,
        ThreadPool
    }

    public enum DataFormat
    {
        Xml,
        Csv
    }
}