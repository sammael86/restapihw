﻿using System;
using System.Collections.Generic;
using CommandLine;
using System.Diagnostics;
using ParallelHW.Core.Entities;
using ParallelHW.Core.Generators;
using ParallelHW.Core.Loaders;
using ParallelHW.Core.Parsers;
using ParallelHW.DataAccess.Parsers;
using ParallelHW.DataGenerator.Generators;
using ParallelHW.Loader.Loaders;

namespace ParallelHW.Loader
{
    internal static class Program
    {
        private static string _dataFilePath;
        private static string _method;
        private static int _dataCount;
        private static int _threadsCount;
        private static DataFormat _dataFormat;
        private static ThreadType _threadType;

        private static void Main(string[] args)
        {
            if (LoadOptions(args).Tag == ParserResultType.NotParsed)
                return;

            if (args.Length == 0)
                Console.WriteLine("Program started with default arguments. For help run the program with --help");

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var sw = Stopwatch.StartNew();
            if (_method.ToLower() == "method")
            {
                Console.WriteLine("Generating data file by method...");
                AbstractDataGenerator generator = new Generator(_dataCount, _dataFilePath);

                if (_dataFormat == DataFormat.Xml)
                    generator = new XmlGenerator(generator);
                if (_dataFormat == DataFormat.Csv)
                    generator = new CsvGenerator(generator);

                generator.Generate();
            }
            else
            {
                Console.WriteLine("Generating data file by process...");
                var process = new ProcessStartInfo
                {
                    FileName = _method,
                    ArgumentList =
                    {
                        $"-n {_dataFilePath}",
                        $"-d {_dataCount}",
                        $"-f {_dataFormat}"
                    }
                };
                try
                {
                    Process.Start(process)?.WaitForExit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error! {ex.Message}");
                    return;
                }
            }

            sw.Stop();
            Console.WriteLine($"Data file generated in {sw.ElapsedMilliseconds} ms");

            sw = Stopwatch.StartNew();
            IDataParser<List<Customer>> parser = default;

            if (_dataFormat == DataFormat.Xml)
                parser = new XmlParser(_dataFilePath);
            if (_dataFormat == DataFormat.Csv)
                parser = new CsvParser(_dataFilePath);

            var customers = parser.Parse();
            sw.Stop();
            Console.WriteLine($"File parsed in {sw.ElapsedMilliseconds} ms");


            IDataLoader loader = default;
            if (_threadType == ThreadType.Thread)
                loader = new DataLoader(customers, _threadsCount);
            if (_threadType == ThreadType.ThreadPool)
                loader = new ThreadPoolDataLoader(customers, _threadsCount);
            loader.LoadData();
        }

        private static ParserResult<Options> LoadOptions(string[] args)
            => Parser.Default.ParseArguments<Options>(args)
                .WithParsed(o =>
                {
                    _dataFilePath = o.DataFilePath;
                    _method = o.Method;
                    _dataCount = o.DataCount;
                    _threadsCount = o.ThreadsCount;
                    _dataFormat = o.DataFormat;
                    _threadType = o.ThreadType;
                });
    }
}