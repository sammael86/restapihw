﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using ParallelHW.Core.Entities;
using ParallelHW.Core.Loaders;
using ParallelHW.DataAccess.Repositories;

namespace ParallelHW.Loader.Loaders
{
    public class ThreadPoolDataLoader : IDataLoader
    {
        private readonly Customer[] _customers;
        private readonly int _threadsCount;

        public void LoadData()
        {
            Console.WriteLine("Loading data from List<Customer> to DB");

            var sw = Stopwatch.StartNew();

            var pieceSize = 1f * _customers.Length / _threadsCount;
            var handles = new ManualResetEvent[_threadsCount];
            for (var i = 0; i < _threadsCount; i++)
            {
                var startIndex = (int) Math.Floor(i * pieceSize);
                var endIndex = (int) Math.Floor((i + 1) * pieceSize - 1);
                handles[i] = new ManualResetEvent(false);
                var currentHandle = handles[i];

                void Action()
                {
                    LoadCustomer(startIndex, endIndex);
                    currentHandle.Set();
                }

                ThreadPool.QueueUserWorkItem(state => Action());
            }

            WaitHandle.WaitAll(handles);

            sw.Stop();

            Console.WriteLine($"Data saved to DB in {sw.ElapsedMilliseconds} ms");
            Console.WriteLine("Loaded data from List<Customer> to DB");
        }

        private void LoadCustomer(int startIndex, int endIndex)
        {
            var r = new Random().Next(15000, 50000);
            var addText = r > (endIndex - startIndex) ? $" , saving each {r} items," : "";
            Console.WriteLine(
                $" Thread #{Thread.CurrentThread.ManagedThreadId} work started{addText} from {startIndex} to {endIndex}");

            var customerRepository = new CustomerRepository();
            for (var i = startIndex; i <= endIndex; i++)
            {
                customerRepository.AddCustomer(_customers[i]);
                if (i % r == 0)
                    TrySave(customerRepository);
            }

            TrySave(customerRepository);
            Console.WriteLine($" Thread #{Thread.CurrentThread.ManagedThreadId} work finished");
        }

        private void TrySave(CustomerRepository customerRepository)
        {
            do
            {
                try
                {
                    customerRepository.Save();
                    return;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($" Warning! Thread #{Thread.CurrentThread.ManagedThreadId}. {ex.Message}");
                    Task.Delay(500).Wait();
                }
            } while (true);
        }

        public ThreadPoolDataLoader(List<Customer> customers, int threadsCount)
        {
            _customers = customers.ToArray();
            _threadsCount = threadsCount;
        }
    }
}